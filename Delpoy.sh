#!/bin/bash
set -e

if [[ -z $1 || $1 == "--help" ]]; then
    echo "deploy.sh [mode: -f for full -h for hotfix]  [emergency: -e1 , skip warmup | -e2 , skip delay] [version]"
    echo "example : emergency : -nr & mode : -f => deploy.sh -f -nr 1322 (all the options alre required)"
    echo "-nr will not"
    exit
fi

registry="registry:5000"
image="ajc-swarm"
tag=0
version=''
if [ -z $3 ]; then
    version=$2
else 
    version=$3
fi

calc(){
    val1=`cat ind.ind`
    val2=$(echo $val1 |grep -o '^[0-9]*_')
    vesrion=$(echo $val2 |grep -o '\d+(?=_)')
    val3=$(echo $val1 |grep -o '_[0-9]*')
    hfix=$(echo $val3 |grep -o '[0-9]*')
    ind=`expr $hfix + 1`
    tag=`expr $version"_"$ind`
    command='sed -i 's/^[0-9]*_[0-9]+/$tag/g' ind.ind'

    $command

}

if [[ $1 == "-f" ]]
then
#check for overrwite
    tag=$version
    echo $tag"_0" > ind.ind
    
elif [[ $1 = "-h" ]]
then
    calc
else
    echo "wrong deploy mode : $1"
fi


Cpush(){
    cd "$(dirname "$0")"
    docker build -f update.dockerfile -t "${registry}/${image}" .
    docker push "${registry}/${image}"
    if [ ! -z $3 ]; then
        docker image tag "${registry}/${image}" "${registry}/${image}:${tag}"
        docker push "${registry}/${image}:${tag}"
    fi
}

Cpush


Update(){


    echo "ajc_app service updated."
}

case $2 in
    -e1)
    
        docker service update ajc_app --image ${registry}/${image}:${tag} --health-cmd "echo test-skiped"
        
        ;;
    -e2)

        docker service update ajc_app --image ${registry}/${image}:${tag} --health-cmd 'echo test-skiped' --update-delay 0s
        
        ;;
    *)
        docker service update ajc_app --image ${registry}/${image}:${tag} --health-cmd "curl localhost/health" --update-delay 10s --update-parallelism 1
        
        ;;
    
    esac